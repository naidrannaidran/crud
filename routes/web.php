<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*PARTE PUBLICA*/
Route::get('/', function () {
    return view('index');
});

Route::get('/dashboard', function(){
    return view ('modelo/indexBack');
})->middleware(['auth'])->name('dashboard');

Route::view('/index', 'index')->name('index');
Route::get('/galeria', 'PublicController@listarGaleria')->name('galeria');
Route::get('/rutas', 'PublicController@listarRutas')->name('rutas');
Route::get('/tutoriales', 'PublicController@listarTutoriales')->name('tutoriales');
Route::get('/tienda', 'PublicController@listarTienda')->name('tienda');
Route::get('/tecnica', 'PublicController@listarTecnicas')->name('tecnica');

/*PARTE PRIVADA*/
Auth::routes();
Route::view('/modelo', 'modelo.indexBack')->name('indexBack');

/*Route::view('/modelo', 'index')->name('indexBack');
Route::view('/modelo/galeria', 'modelo/Galeria/galeria-index')->name('galeriaBack');
Route::view('/modelo/rutas', 'modelo/rutas')->name('rutasBack');
Route::view('/modelo/tutoriales', 'modelo/tutoriales')->name('tutorialesBack');
Route::view('/modelo/tienda', 'modelo/tienda')->name('tiendaBack');
Route::view('/modelo/tecnica', 'modelo/tecnicas')->name('tecnicasBack');*/

/*Usuarios*/
Route::get('/modelo/usuario','UsuarioController@index')->name('usuario-index');
Route::get('/modelo/usuario/create','UsuarioController@create')->name('usuario-create');
Route::post('/modelo/usuario/store', 'UsuarioController@store')->name('usuario-store');
Route::get('/modelo/usuario/show/{id}', 'UsuarioController@show')->name('usuario-show');
Route::get('/modelo/usuario/edit/{id}', 'UsuarioController@edit')->name('usuario-edit');
Route::post('/modelo/usuario/update/{id}', 'UsuarioController@update')->name('usuario-update');
Route::any('/modelo/usuario/destroy/{id}', 'UsuarioController@destroy')->name('usuario-destroy');

/*Galeria*/
Route::get('/modelo/galeria','GaleriaController@index')->name('galeria-index');
Route::get('/modelo/galeria/create','GaleriaController@create')->name('galeria-create');
Route::post('/modelo/galeria/store', 'GaleriaController@store')->name('galeria-store');
Route::get('/modelo/galeria/show/{id}', 'GaleriaController@show')->name('galeria-show');
Route::get('/modelo/galeria/edit/{id}', 'GaleriaController@edit')->name('galeria-edit');
Route::post('/modelo/galeria/update/{id}', 'GaleriaController@update')->name('galeria-update');
Route::any('/modelo/galeria/destroy/{id}', 'GaleriaController@destroy')->name('galeria-destroy');


/*Rutas*/
Route::get('/modelo/rutas','RutasController@index')->name('rutas-index');
Route::get('/modelo/rutas/create','RutasController@create')->name('rutas-create');
Route::post('/modelo/rutas/store', 'RutasController@store')->name('rutas-store');
Route::get('/modelo/rutas/show/{id}', 'RutasController@show')->name('rutas-show');
Route::get('/modelo/rutas/edit/{id}', 'RutasController@edit')->name('rutas-edit');
Route::post('/modelo/rutas/update/{id}', 'RutasController@update')->name('rutas-update');
Route::any('/modelo/rutas/destroy/{id}', 'RutasController@destroy')->name('rutas-destroy');

/*Tutoriales*/
Route::get('/modelo/tutoriales','TutorialesController@index')->name('tutoriales-index');
Route::get('/modelo/tutoriales/create','TutorialesController@create')->name('tutoriales-create');
Route::post('/modelo/tutoriales/store', 'TutorialesController@store')->name('tutoriales-store');
Route::get('/modelo/tutoriales/show/{id}', 'TutorialesController@show')->name('tutoriales-show');
Route::get('/modelo/tutoriales/edit/{id}', 'TutorialesController@edit')->name('tutoriales-edit');
Route::post('/modelo/tutoriales/update/{id}', 'TutorialesController@update')->name('tutoriales-update');
Route::any('/modelo/tutoriales/destroy/{id}', 'TutorialesController@destroy')->name('tutoriales-destroy');

/*Tecnicas*/
Route::get('/modelo/tecnicas','TecnicaController@index')->name('tecnica-index');
Route::get('/modelo/tecnicas/create','TecnicaController@create')->name('tecnica-create');
Route::post('/modelo/tecnicas/store', 'TecnicaController@store')->name('tecnica-store');
Route::get('/modelo/tecnicas/show/{id}', 'TecnicaController@show')->name('tecnica-show');
Route::get('/modelo/tecnicas/edit/{id}', 'TecnicaController@edit')->name('tecnica-edit');
Route::post('/modelo/tecnicas/update/{id}', 'TecnicaController@update')->name('tecnica-update');
Route::any('/modelo/tecnicas/destroy/{id}', 'TecnicaController@destroy')->name('tecnica-destroy');

/*Tienda*/
Route::get('/modelo/tienda','TiendaController@index')->name('tienda-index');
Route::get('/modelo/tienda/create','TiendaController@create')->name('tienda-create');
Route::post('/modelo/tienda/store', 'TiendaController@store')->name('tienda-store');
Route::get('/modelo/tienda/show/{id}', 'TiendaController@show')->name('tienda-show');
Route::get('/modelo/tienda/edit/{id}', 'TiendaController@edit')->name('tienda-edit');
Route::post('/modelo/tienda/update/{id}', 'TiendaController@update')->name('tienda-update');
Route::any('/modelo/tienda/destroy/{id}', 'TiendaController@destroy')->name('tienda-destroy');


/*Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/


Route::get('/home', 'HomeController@index')->name('home');
