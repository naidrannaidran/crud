$(document).ready(function(){
    $("#busqueda").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".card-group .card").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });