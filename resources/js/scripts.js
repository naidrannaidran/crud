$(window).scroll(function(){
    if ($(window).scrollTop() >= 360) {
        $('nav').addClass('fixed-header');
        $('nav div').addClass('visible-title');
    }
    else {
        $('nav').removeClass('fixed-header');
        $('nav div').removeClass('visible-title');
    }
});

$(document).ready(function(){
    $(".container").fadeIn(1000);
    $(".s2class").css({"color":"#EE9BA3"});
    $(".s1class").css({"color":"#748194"});
    $("#left").removeClass("left_hover");
    $("#right").addClass("right_hover");
    $(".signin").css({"display":"none"});
    $(".signup").css({"display":""});
});
$("#right").click(function(){
    $("#left").removeClass("left_hover");
    $(".s2class").css({"color":"#EE9BA3"});
    $(".s1class").css({"color":"#748194"});
    $("#right").addClass("right_hover");
    $(".signin").css({"display":"none"});
    $(".signup").css({"display":""});
});
$("#left").click(function(){
    $(".s1class").css({"color":"#EE9BA3"});
    $(".s2class").css({"color":"#748194"});
    $("#right").removeClass("right_hover");
    $("#left").addClass("left_hover");
    $(".signup").css({"display":"none"});
    $(".signin").css({"display":""});
});

$(document).ready(function(){
    $("#boton").click(function (){
        $("#myLinks li").toggle();
    });
});