<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"
    <meta name="title" content="Foro de la página de Photomancy">
    <meta name="description"
          content="Foro en el que se encuentran los principales hilos de preguntas y respuestas referentes a temas de fotografía">
    <meta name="keywords" content="photomancy, foro, preguntas, respuestas, fotografia, imagen">
    <meta name="author" content="Adrian Moreno Rodriguez">
    <link rel="stylesheet" type="text/css" href="{{asset('css/estilos.css')}}">

    <link rel="stylesheet" href="{{asset('https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css')}}"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>

    <script src="js/scripts.js"></script>
</head>
<body>
@include('includes.nav')

<div class="container">
    <div class="infoVideo">
        <h3 class="title">Tutorial 1</h3>
        <p>Como hacer 3 retratos en 5 minutos en espacios exteriores</p>
    </div>
    <video src="https://www.youtube.com/watch?v=w5EdNjpcDZ8&list=PLWdeD0PPsBr6QZuvk384eRm56lknbzbsj" controls
           preload="auto"></video>
</div>
<div class="container">
    <div class="infoVideo">
        <h3 class="title">Tutorial 2</h3>
        <p>Como ajustar el enfoque manualmente para obtener una mayor nitidez - Tutorial 2</p>
    </div>
    <video src="https://www.youtube.com/watch?v=w5EdNjpcDZ8&list=PLWdeD0PPsBr6QZuvk384eRm56lknbzbsj" controls
           preload="auto"></video>
</div>
<div class="container">
    <div class="infoVideo">
        <h3 class="title">Tutorial 3</h3>
        <p>Modo retrato. Guia básica de los conceptos principales - Tutorial 3</p>
    </div>
    <video src="https://www.youtube.com/watch?v=iE8-4zKsYpk&list=PLWdeD0PPsBr5BUcR5XX7JIPc4_k_bX3dP" controls
           preload="auto"></video>
</div>
<div class="container">
    <div class="infoVideo">
        <h3 class="title">Tutorial 4</h3>
        <p>ISO. Conceptos generales para fotógrafos principiantes - Tutorial 4</p>
    </div>
    <video src="https://www.youtube.com/watch?v=sBOHE_E1QMs&list=PLWdeD0PPsBr5BUcR5XX7JIPc4_k_bX3dP&index=4" controls
           preload="auto"></video>
</div>

@include('includes.footer')

</body>
</html>