<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Página principal de Photomancy">
    <meta name="description"
          content="Pagina principal del proyecto Photomancy, la cual da acceso al resto de elementos de la web">
    <meta name="keywords" content="photomancy, index, home, fotografia, imagen">
    <meta name="author" content="Adrian Moreno Rodriguez">
    <script src="js/scripts.js"></script>


    <link rel="stylesheet" type="text/css" href="{{asset('css/estilos.css')}}">

</head>
<body>
@include('includes.nav')
<div class="info">
    <p>Photomancy nace de la necesidad de dar a los fotográfos un lugar donde poder desarrollar sus actividades de una
        forma más social. Proporcionamos una plataforma única para que cada persona, aficionada o no, sea capaz de
        expandir sus horizontes gracias a recursos como tutoriales, foros de ayuda, trucos o material de fotografía. En
        Photomancy podrás encontrar a gente con tus mismas inquietudes, sueños y aspiraciones para dotar al mundo de una
        imagen, un relato de lo que muchas veces nuestros ojos ven. En definitiva, un sitio donde completar tu perfil de
        fotógrafo.</p>
    <p class="quote"><i>“Fotografiar es colocar la cabeza, el ojo y el corazón en un mismo eje”. Henri
            Cartier-Bresson</i></p>
</div>
<div id="captioned-gallery">
    <figure class="slider">
        <figure>
            <img src="img/paisaje5.jpg" alt>
            <figcaption>Real Monasterio de San Lorenzo de El Escorial</figcaption>
        </figure>
        <figure>
            <img src="img/paisaje2.jpg" alt>
            <figcaption>Wanaka, New Zealand</figcaption>
        </figure>
        <figure>
            <img src="img/paisaje3.jpg" alt>
            <figcaption>Utah, United States</figcaption>
        </figure>
        <figure>
            <img src="img/paisaje4.jpg" alt>
            <figcaption>Bryce Canyon, Utah, United States</figcaption>
        </figure>
        <figure>
                <img src="img/logoProyectoDiciembre3.png" alt>
            <figcaption>Hobbiton, New Zealand</figcaption>
        </figure>
    </figure>
</div>

<div class="containerIndexHistoria">
    <h3>Historia de la fotografía</h3>
    <p>La historia de la fotografía empieza oficialmente en 1839, con la divulgación mundial del primer procedimiento
        fotográfico: el daguerrotipo. Dos de los antecedentes de la fotografía son: la cámara oscura, que ya mencionaba
        Aristóteles para la observación los eclipses solares, y las investigaciones sobre las sustancias
        fotosensibles, especialmente el ennegrecimiento de las sales de plata, que actuaban con la acción de la luz. Ibn
        al-Haytham (Alhazen) llevó varios experimentos sobre la cámara oscura y la cámara estenopeica. También hizo
        experimentos Cesare Cesanino, alumno de Leonardo da Vinci y el primero que realizó una publicación sobre la
        cámara oscura en 1521</p>
    <p>La fotografía en color fue experimentada durante el siglo XIX, pero no tuvo aplicaciones comerciales, por su
        dificultad e imperfección. Los experimentos iniciales no fueron capaces de conseguir que los colores quedaran
        fijados en la fotografía. La primera fotografía en color fue obtenida por el físico y matemático escocés James
        Clerk Maxwell el cual formuló la teoría clásica del electromagnetismo. Sus ecuaciones permitieron demostrar que
        la electricidad, el magnetismo y la luz son manifestaciones de un mismo fenómeno: el campo electromagnético.</p>
</div>
<div class="tituloApps"><h5>Principales Apps de fotografía / edición fotográfica</h5></div>
<div class="containerIndexApps">
    <div class="card">
        <img src="{{asset('img/photoshop.png')}}">
        <h3>Photoshop</h3>

    </div>
    <div class="card">
    <img src="{{asset('img/illustrator.png')}}">
        <h3>Adobe Illustrator</h3>

    </div>
    <div class="card">
    <img src="{{asset('img/lightroom.png')}}">
        <h3>Adobe Lightroom</h3>

    </div>
    <div class="card">
    <img src="{{asset('img/Curso-Capture-One.png')}}">
        <h3>Capture One</h3>
    </div>
</div>
@include('includes.footer')

</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="js/scripts.js"></script>

</html>