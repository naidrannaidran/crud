<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <title>Users Show</title>
</head>
<body>
    <form method="get" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_ruta" value="{{ $rutas->nombre_ruta }}" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="descripcion_ruta" value="{{ $rutas->descripcion_ruta }}" placeholder="FOTO">
        <input class="form-contact-input" type="text" name="id_ruta" value="{{ $rutas->id_ruta }}" placeholder="RUTA">
     </form>
</body>
</html>
