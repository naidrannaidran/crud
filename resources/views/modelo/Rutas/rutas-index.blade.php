<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Galeria</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Galeria de imagenes de Photomancy">
    <meta name="description"
          content="Galeria de imagenes en la que se muestran las fotos de muestra de la pagina web">
    <meta name="keywords" content="photomancy, galeria, imagenes, fotografia, rutas">
    <meta name="author" content="Adrian Moreno Rodriguez">


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    
    <!--link rel="stylesheet" type="text/css" href="{{asset('css/estilosBack.css')}}"-->
    <script src="js/scripts.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.js"
	integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM="
	crossorigin="anonymous"></script>
</head>
<body>
    @include('layouts.plantillaBuena')
    @include('layouts.includes.navbar')
    <input type="text" id="busqueda" placeholder="Buscar">
    <br> 
    <br>
    <br> 
    @foreach ($rutas as $u)
        <a href="{{ route('rutas-show', $u->id) }}"><p>ID: {{ $u->id }}</p></a>
        <p>NOMBRE: {{ $u->nombre_ruta }}</p>
        <p>DESCRIPCION: {{ $u->descripcion_ruta }}</p>
        <p>MAPA:</p> <img src='{{ asset("storage/$u->mapa_ruta") }}'>
        <a href="{{ route('rutas-create', $u->id) }}">Insertar ruta</a>
        <a href="{{ route('rutas-edit', $u->id) }}">Editar ruta</a>
        <a href="{{ route('rutas-destroy', $u->id) }}">Eliminar ruta</a>
        
        <br><br><br>
    @endforeach
</body>
</html>
