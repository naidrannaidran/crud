<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('rutas-update', $rutas->id) }}" enctype="multipart/form-data" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_ruta" value="{{ $rutas->nombre_ruta }}" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="descripcion_ruta" value="{{ $rutas->descripcion_ruta }}" placeholder="DESCRIPCION">
        <label class="form-contact-input" for="mapa_ruta"><input type="file" name="mapa_ruta" value="{{ $rutas->mapa_ruta }}" placeholder="MAPA">
        <button type="submit">Actualizar</button>
     </form>
</body>
</html>