<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('rutas-store') }}" enctype="multipart/form-data" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_ruta" class="form-control" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="descripcion_ruta" class="form-control" placeholder="DESCRIPCION">
        <input class="form-contact-input" type="file" name="mapa_ruta" class="form-control" placeholder="MAPA">
        <button type="submit">Enviar</button>
     </form>
</body>
</html>
