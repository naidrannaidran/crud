@extends('layouts.plantilla')

@section('content')
    @foreach ($tutoriales as $u)
        <a href="{{ route('tutoriales-show', $u->id) }}"><p>ID: {{ $u->id }}</p></a>
        <p>NOMBRE: {{ $u->nombre_tutorial }}</p>
        <p>DESCRIPCIÓN: {{ $u->descripcion_tutorial }}</p>
        <p>VIDEO:</p> <img src='{{ asset("storage/$u->video_tutorial") }}'>
        <a href="{{ route('tutoriales-create', $u->id) }}">Insertar tutoriales</a>
        <a href="{{ route('tutoriales-edit', $u->id) }}">Editar tutoriales</a>
        <a href="{{ route('tutoriales-destroy', $u->id) }}">Eliminar tutoriales</a>
        
        <br><br><br>
    @endforeach
@endsection
