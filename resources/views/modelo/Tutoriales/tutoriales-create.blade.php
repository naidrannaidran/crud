<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('tutoriales-store') }}" enctype="multipart/form-data" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_tutorial" class="form-control" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="descripcion_tutorial" class="form-control" placeholder="DESCRIPCIÓN">
        <label class="form-contact-input" for="video_tutorial"><input type="file" name="video_tutorial" class="form-control" placeholder="VIDEO">
        <button type="submit">Enviar</button>
     </form>
</body>
</html>
