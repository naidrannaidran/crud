<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="get" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_tutorial" value="{{ $tutoriales->nombre_tutorial }}" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="descripcion_tutorial" value="{{ $tutoriales->descripcion_tutorial }}" placeholder="DESCRIPCIÓN">
        <input class="form-contact-input" type="text" name="video_tutorial" value="{{ $tutoriales->video_tutorial }}" placeholder="VIDEO">
     </form>
</body>
</html>
