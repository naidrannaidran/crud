<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Página principal de Photomancy">
    <meta name="description"
          content="Pagina principal del proyecto Photomancy, la cual da acceso al resto de elementos de la web">
    <meta name="keywords" content="photomancy, index, home, fotografia, imagen">
    <meta name="author" content="Adrian Moreno Rodriguez">



    <link rel="stylesheet" type="text/css" href="{{asset('css/estilos.css')}}">

</head>
<body>
@include('modelo.includesBack.navBack')
<div>
<p>Apartado de gestión del proyecto. En estas vistas se da acceso a los formularios y a las acciones pertinentes para gestionar los objetos que conforman
el CRUD para esta práctica de Laravel</p>
</div>
<div>
<p>Se han enlazado las vistas, las cuales son diferentes ya que en el apartado frontal no se tiene acceso a dicha gestión de los elementos.
Se han conservado los estilos de Laravel en el apartado de los listados, de forma que se combina el css original del proyecto (front y partes del back)
con los de Laravel en esta parte de gestión.</p><br>
</div>
@include('modelo.includesBack.footerBack')
</body>
</html>