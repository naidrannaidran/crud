@extends('layouts.plantilla')

@section('content')
    @foreach ($usuario as $u)
        <a href="{{ route('usuario-show', $u->id) }}"><p>ID: {{ $u->id }}</p></a>
        <p>NOMBRE: {{ $u->nombre_usuario}}</p>
        <p>EMAIL: {{ $u->email_usuario}}</p>
        <a href="{{ route('usuario-create', $u->id) }}">Insertar usuario</a>
        <a href="{{ route('usuario-edit', $u->id) }}">Editar usuario</a>
        <a href="{{ route('usuario-destroy', $u->id) }}">Eliminar usuario</a>
        
        <br><br><br>
    @endforeach
@endsection
