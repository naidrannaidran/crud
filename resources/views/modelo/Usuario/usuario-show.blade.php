<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Users Show</title>
</head>
<body>
    <form method="get" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="name" value="{{ $usuario->name }}" placeholder="NOMBRE">
        <input class="form-contact-input" type="email" name="email" value="{{ $usuario->email }}" placeholder="EMAIL">
        <input class="form-contact-input" type="password" name="password" value="{{ $usuario->password }}" placeholder="CONTRASEÑA">
     </form>
</body>
</html>
