<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('usuario-update', $usuario->id) }}" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="name" value="{{ $usuario->nombre_usuario }}" placeholder="NOMBRE">
        <input class="form-contact-input" type="email" name="email" value="{{ $usuario->email_usuario }}" placeholder="EMAIL">
        <input class="form-contact-input" type="password" name="password" value="{{ $usuario->contrasena_usuario }}" placeholder="CONTRASEÑA">
        <button type="submit">Actualizar</button>
     </form>
</body>
</html>
