<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('usuario-store') }}" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_usuario" class="form-control" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="apellidos_usuario" class="form-control" placeholder="APELLIDOS">
        <input class="form-contact-input" type="password" name="contrasena_usuario" class="form-control" placeholder="CONTRASEÑA">
        <input class="form-contact-input" type="email" name="email_usuario" class="form-control" placeholder="EMAIL">
        <input class="form-contact-input" type="text" name="alias_usuario" class="form-control" placeholder="ALIAS">
        <input class="form-contact-input" type="text" name="foto_usuario" class="form-control" placeholder="FOTO">
        <input class="form-contact-input" type="text" name="permiso_usuario" class="form-control" placeholder="PERMISO">
        <input class="form-contact-input" type="text" name="id_tutorial" class="form-control" placeholder="ID ASOCIADO">
        <button type="submit">Enviar</button>
     </form>
</body>
</html>
