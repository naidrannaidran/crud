<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('galeria-store') }}" enctype="multipart/form-data" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_foto" class="form-control" placeholder="NOMBRE">
        <label for="ruta_foto"><input class="form-contact-input" type="file" name="ruta_foto" class="form-control">
        <input class="form-contact-input" type="text" name="id_ruta" class="form-control" placeholder="RUTA ASOCIADA">
        <button type="submit">Enviar</button>
     </form>
</body>
</html>