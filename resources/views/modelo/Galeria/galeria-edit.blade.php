<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('galeria-update', $galeria->id) }}" class="form-contact" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_foto" value="{{ $galeria->nombre_foto }}" placeholder="NOMBRE">
        <label for="ruta_foto"><input type="file" name="ruta_foto" class="form-contact-input" value="{{ $galeria->ruta_foto }}" >
        <input class="form-contact-input" type="text" name="id_ruta" value="{{ $galeria->id_ruta }}" placeholder="RUTA ASOCIADA">
        <button type="submit">Actualizar</button>
     </form>
</body>
</html>