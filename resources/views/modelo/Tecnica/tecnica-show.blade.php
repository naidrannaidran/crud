<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
<body>
    <form method="get" class="form-contact">
        {{ csrf_field() }}
        <input type="text" name="nombre_tecnica" value="{{ $tecnica->nombre_tecnica }}" placeholder="NOMBRE">
        <input type="text" name="descripcion_tecnica" value="{{ $tecnica->descripcion_tecnica }}" placeholder="DESCRIPCIÓN">
        <input type="text" name="foto_tecnica" value="{{ $tecnica->foto_tecnica }}" placeholder="FOTO">
     </form>
</body>
</html>
