<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('tecnica-store') }}" enctype="multipart/form-data" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact" type="text" name="nombre_tecnica" class="form-control" placeholder="NOMBRE">
        <input class="form-contact" type="text" name="descripcion_tecnica" class="form-control" placeholder="DESCRIPCIÓN">
        <label for="foto_tecnica"><input class="form-contact" type="file" name="foto_tecnica" class="form-control" placeholder="FOTO">
        <button type="submit">Enviar</button>
     </form>
</body>
</html>
