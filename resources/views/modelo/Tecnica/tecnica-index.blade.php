<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Galeria</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Galeria de imagenes de Photomancy">
    <meta name="description"
          content="Galeria de imagenes en la que se muestran las fotos de muestra de la pagina web">
    <meta name="keywords" content="photomancy, galeria, imagenes, fotografia, rutas">
    <meta name="author" content="Adrian Moreno Rodriguez">


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    
    <!--link rel="stylesheet" type="text/css" href="{{asset('css/estilosBack.css')}}"-->
    <script src="js/scripts.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.js"
	integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM="
	crossorigin="anonymous"></script>
</head>
<body>
    @include('layouts.plantillaBuena')
    @include('layouts.includes.navbar')
    <input type="text" id="busqueda" placeholder="Buscar">
    <br> 
    <br>
    <br> 
    @foreach ($tecnica as $u)
        <a href="{{ route('tecnica-show', $u->id) }}"><p>ID: {{ $u->id }}</p></a>
        <p>NOMBRE: {{ $u->nombre_tecnica }}</p>
        <p>DESCRIPCIÓN: {{ $u->descripcion_tecnica }}</p>
        <p>FOTO:</p> <img src='{{ asset("storage/$u->foto_tecnica") }}'>
        <a href="{{ route('tecnica-create', $u->id) }}">Insertar tecnica</a>
        <a href="{{ route('tecnica-edit', $u->id) }}">Editar tecnica</a>
        <a href="{{ route('tecnica-destroy', $u->id) }}">Eliminar tecnica</a>
        
        <br><br><br>
    @endforeach
</body>
</html>
