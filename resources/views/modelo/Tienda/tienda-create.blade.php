<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>
    <form method="POST" action="{{ route('tienda-store') }}" enctype="multipart/form-data" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_articulo" class="form-control" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="descripcion_articulo" class="form-control" placeholder="DESCRIPCIÓN">
        <label for="foto_articulo"><input class="form-contact-input" type="file" name="foto_articulo" class="form-control" placeholder="FOTO">
        <input class="form-contact-input" type="text" name="precio_articulo" class="form-control" placeholder="PRECIO">
        <button type="submit">Enviar</button>
     </form>
</body>
</html>
