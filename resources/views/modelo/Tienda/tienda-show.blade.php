<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
    <link href='{{asset("css/estilos.css")}}' rel="stylesheet" type="text/css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector: 'textarea'});</script>
</head>
<body>>
<body>
    <form method="get" class="form-contact">
        {{ csrf_field() }}
        <input class="form-contact-input" type="text" name="nombre_articulo" value="{{ $tienda->nombre_articulo }}" placeholder="NOMBRE">
        <input class="form-contact-input" type="text" name="descripcion_articulo" value="{{ $tienda->descripcion_articulo }}" placeholder="DESCRIPCIÓN">
        <input class="form-contact-input" type="text" name="foto_articulo" value="{{ $tienda->foto_articulo }}" placeholder="FOTO">
        <input class="form-contact-input" type="text" name="precio_articulo" value="{{ $tienda->precio_articulo }}" placeholder="PRECIO">
     </form>
</body>
</html>
