@extends('layouts.plantilla')

@section('content')
    @foreach ($tienda as $u)
        <a href="{{ route('tienda-show', $u->id) }}"><p>ID: {{ $u->id }}</p></a>
        <p>NOMBRE: {{ $u->nombre_articulo }}</p>
        <p>DESCRIPCIÓN: {{ $u->descripcion_articulo }}</p>
        <p>FOTO:</p> <img src='{{ asset("storage/$u->foto_articulo") }}'>
        <p>PRECIO: {{ $u->precio_articulo }}</p>
        <a href="{{ route('tienda-edit', $u->id) }}">Editar tienda</a>
        <a href="{{ route('tienda-destroy', $u->id) }}">Eliminar tienda</a>
        
        <br><br><br>
    @endforeach
@endsection
