<header>
    <div class="header-banner">
        <h1>Photomancy</h1>
    </div>
    <div class="clear"></div>
    <nav class="navCerrado">
        <div class="site-title">Photomancy</div>

        <ul id="myLinks">
            <span class="botonAbierto botonCerrado" id="boton" onclick="openNav()">&#9776;</span>

            <!-- <li><a href="{{route('indexBack')}}">Home</a></li> -->
            <li><a href="{{route('galeria-index')}}">Galería</a></li>
            <li><a href="{{route('rutas-index')}}">Rutas</a></li>
            <li><a href="{{route('tutoriales-index')}}">Tutoriales</a></li>
            <li><a href="{{route('tienda-index')}}">Tienda</a></li>
            <li><a href="{{route('tecnica-index')}}">Técnica</a></li>
            </ul>
    </nav>
</header>