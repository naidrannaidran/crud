<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Rutas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"
    <meta name="title" content="Página principal de Photomancy">
    <meta name="description"
          content="Pagina principal del proyecto Photomancy, la cual da acceso al resto de elementos de la web">
    <meta name="keywords" content="photomancy, index, home, fotografia, imagen">
    <meta name="author" content="Adrian Moreno Rodriguez">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('css/estilos.css')}}">
    <script src="js/scripts.js"></script>
</head>
<body class="routeBody">
@include('includes.nav')

@foreach ($rutas as $u)
<div class="containerRuta">
    <div class="infoRuta">
        <a href="{{ route('rutas-show', $u->id) }}">
        <h3 class="titleRuta">{{ $u->nombre_ruta }}</h3>
        <p>{{ $u->descripcion_ruta }}</p>
        <img src='{{asset("storage/$u->mapa_ruta")}}'>
    </div>
    <img src="img/rutaSanLorenzoEscorial.png" alt="">
</div>
@endforeach

@include('includes.footer')

</body>
</html>