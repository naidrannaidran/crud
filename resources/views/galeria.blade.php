<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Galeria</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"
    <meta name="title" content="Galeria de imagenes de Photomancy">
    <meta name="description"
          content="Galeria de imagenes en la que se muestran las fotos de muestra de la pagina web">
    <meta name="keywords" content="photomancy, galeria, imagenes, fotografia, rutas">
    <meta name="author" content="Adrian Moreno Rodriguez">


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('css/estilos.css')}}">
    <script src="js/scripts.js"></script>
</head>
<body>
@include('includes.nav')
@foreach ($galeria as $u)
<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
<div class="containerGaleria">
    <div class="card">
        <img src='{{asset("storage/$u->ruta_foto")}}'>
        <h3 class="titleRuta">{{ $u->nombre_foto}}</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1494122353634-c310f45a6d3c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ecd4e63ddbfbc55465d3d0abc16ec5c5&auto=format&fit=crop&w=2850&q=80"/>
        <h3>Tokyo, Distrito Central</h3>
    </div>

    <div class="card">
        <img src="https://images.unsplash.com/photo-1503867913710-3f1656e94ac9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=4ec81722964d9d4b89884c82bcb11642&auto=format&fit=crop&w=1027&q=80"/>
        <h3>Amanecer en Kenia</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1533756972958-d6f38a9761e3?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d710493bc1818842db8ce47f887708f2&auto=format&fit=crop&w=2089&q=80"/>
        <h3>Montañas del sur, Mongolia</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1502957291543-d85480254bf8?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=220d7cdb8357ec667ed2c4d4a3de3eb0&auto=format&fit=crop&w=1567&q=80"/>
        <h3>Cielo nocturno, Hawaii</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1533770699395-5761e5d08106?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=363f39b71ee57e936da6f04a7830895f&auto=format&fit=crop&w=1949&q=80"/>
        <h3>Parque Nacional de Yellowstone</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1533806481099-93f1242aea1e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0f2ba53e0ed1f9ecd16acb04ad0f70f7&auto=format&fit=crop&w=1950&q=80"/>
        <h3>Fiordo de Hrundel</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1533738630286-f1f4a61705f8?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5e2bc6bb33302df3a49d62983fe535ef&auto=format&fit=crop&w=800&q=60"/>
        <h3>Muro viejo, Sicilia</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1533762385849-5aa14c83dbaf?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=040eec84d37ae33312c1bda4d0dfb0f6&auto=format&fit=crop&w=800&q=60"/>
        <h3>Aguas cristalinas, Cuba</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1533754434873-c60f30acf0ca?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=7fd34022d3c026a5cdebba00b4fa89a4&auto=format&fit=crop&w=800&q=60"/>
        <h3>Sierra Nevada, España</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/photo-1533695355243-80ed280a24c1?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f3b50d474b326a034ccf751b3355704a&auto=format&fit=crop&w=800&q=60"/>
        <h3>Almendro en flor, Japón</h3>
    </div>
    <div class="card">
        <img src="https://images.unsplash.com/uploads/1413259835094dcdeb9d3/6e609595?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c82fb4e7054d9fb4211938bf9431ccdf&auto=format&fit=crop&w=1952&q=80"/>
        <h3>Nueva York de noche</h3>
    </div>
</div>
@endforeach

@include('includes.footer')

</body>
</html>