<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Photomancy - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"
    <meta name="title" content="Foro de la página de Photomancy">
    <meta name="description"
          content="Foro en el que se encuentran los principales hilos de preguntas y respuestas referentes a temas de fotografía">
    <meta name="keywords" content="photomancy, foro, preguntas, respuestas, fotografia, imagen">
    <meta name="author" content="Adrian Moreno Rodriguez">
    <link rel="stylesheet" type="text/css" href="{{asset('css/estilos.css')}}">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>

    <script src="js/scripts.js"></script>
</head>
<body class="bodyTec">
@include('includes.nav')
<div id="container">
    <div class="Card">
        <div class="Card-image1"></div>
        <div class="Card-body">
            <h1 class="Card-title">Paisaje</h1>
            <p class="Card-description">El paisaje es la técnica fotográfica más practicada de
                todas, por eso he hecho un artículo específico hablando precisamente de esta. De hecho, el artículo
                trata de los tipos de fotografía de paisaje, ya que dentro de esta técnica fotográfica tenemos
                diferentes opciones (como siempre).
            </p>
            <ul>
                <li>Sin duda alguna, aprender sobre composición fotográfica te va a ayudar a hacer mejores paisajes
                    (aunque la composición te va a servir para cualquier tipo de fotografía).
                </li>
                <li>Cómprate un trípode y aprende a usarlo.
                </li>
                <li>Aprende a usar los diferentes tipos de filtros fotográficos porque en fotografía de paisaje se
                    suelen usar bastantes y muy a menudo (aunque no es obligatorio).
                </li>
                <li>Imprescindible que aprendas sobre la hora azul y la hora dorada.
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="container">
    <div class="Card">
        <div class="Card-image2"></div>
        <div class="Card-body">
            <h1 class="Card-title">Naturaleza</h1>
            <p class="Card-description">
                La fotografía de naturaleza es otra de las técnicas fotográficas más usadas y practicadas de todas.
                Dentro de esta técnica tenemos la fotografía de fauna y la fotografía de flora. Obviamente, existen
                otras modalidades y tipos de fotografía de naturaleza (incluso algunas que veremos más adelante). De
                hecho, un tipo de fotografía de paisaje (el más común de todos, para ser exacto) es, precisamente, un
                tipo de fotografía de naturaleza.
            </p>
            <ul>
                <li>Tener muchísima paciencia es lo primero que vas a necesitar para hacer este tipo de fotografía. Si
                    lo que quieres hacer, concretamente, es fotografía de fauna vas a necesitar no solo paciencia, sino
                    aprender a ser completamente sigiloso.
                </li>
                <li>Tener un teleobjetivo nunca te va a venir mal. O sea que deberías de estudiar la posibilidad de
                    comprarte un nuevo objetivo en caso de no tenerlo.
                </li>
                <li>Aprender sobre el tipo de fauna o flora que vas a fotografiar previamente a la sesión de fotos es
                    una muy buena opción para casi asegurarte un buen disparo.
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="container">
    <div class="Card">
        <div class="Card-image3"></div>
        <div class="Card-body">
            <h1 class="Card-title">Retrato</h1>
            <p class="Card-description">
                El retrato es, muy probablemente, la técnica fotográfica más común de todas (junto a la siguiente que
                vamos a ver).
                Un retrato es una fotografía hecha a una persona. Tan simple como esto.
                Más adelante vamos a ver otro tipo de técnicas fotográficas en las que aparecen personas también y están
                haciendo acciones concretas o son más específicas. Pues bien, si no encaja en ninguna de estas otras
                técnicas fotográficas, es un retrato simple.
            </p>
            <ul>
                <li>Dominar la cámara a la perfección te va a permitir ser capaz de enfrentarte a cualquier situación y
                    aprovechar al máximo las oportunidades exprimiendo todos los conocimientos fotográficos que tengas.
                </li>
                <li>Que aprendas sobre la teoría de la luz y sus características va a permitirte conseguir modelar los
                    rostros de la persona a quien estás fotografiando a un nivel superior, siendo casi totalmente capaz
                    de transmitir cualquier emoción.
                </li>
                <li>Hay otra cosa que puede ser de vital importancia a la hora de hacer retratos. Saber cómo manejar los modelos.
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="container">
    <div class="Card">
        <div class="Card-image4"></div>
        <div class="Card-body">
            <h1 class="Card-title">Nocturna</h1>
            <p class="Card-description">
                La fotografía nocturna es, también, una de las técnicas fotográficas más comunes de todas.
                Generalmente se basa en hacer fotos de noche, tras la puesta de sol y antes del amanecer. No obstante,
                en más del 90% de las ocasiones, cuando se habla de fotografía nocturna no solo nos referimos a esto,
                sino que hablamos de largas exposiciones.
            </p>
            <ul>
                <li>Lo primero de todo es que te familiarices con todos los posibles accesorios fotográficos que vas a
                    necesitar.
                </li>
                <li>Para ser exactos, te recomiendo que empieces a familiarizarte con el intervalómetro, ya que es una
                    herramienta fundamental para este tipo de fotos. Recuerda que todo esto y mucho más lo vemos en el
                    curso de material fotográfico, o sea que échale un vistazo.
                </li>
                <li>Enfocar al infinito debería de ser algo que sepas hacer en cualquier situación, o sea que échale un
                    vistazo a este artículo.
                </li>
                <li>Si lo que quieres es hacer fotografía nocturna de larga exposición, no te olvides de llevarte una
                    silla, mucho café y una linterna para saber por donde andas.
                </li>
            </ul>
        </div>
    </div>
</div>
@include('includes.footer')
</body>
</html>