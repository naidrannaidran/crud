<header>
    <div class="header-banner">
        <h1>Photomancy</h1>
    </div>
    <div class="clear"></div>
    <nav class="navCerrado">
        <div class="site-title">Photomancy</div>

        <ul id="myLinks">
            <span class="botonAbierto botonCerrado" id="boton" onclick="openNav()">&#9776;</span>

            <li><a href="{{route('index')}}">Home</a></li>
            <li><a href="{{route('galeria')}}">Galería</a></li>
            <li><a href="{{route('rutas')}}">Rutas</a></li>
            <li><a href="{{route('tutoriales')}}">Tutoriales</a></li>
            <li><a href="{{route('tienda')}}">Tienda</a></li>
            <li><a href="{{route('tecnica')}}">Técnica</a></li>
            <li><a href="">Login</a></li>
            </ul>
    </nav>
</header>