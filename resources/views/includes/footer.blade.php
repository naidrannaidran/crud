<footer id="colophon" class="site-footer">
    <div class="about">
        <h6>About</h6>
        <p class="text-justify">Photomancy es el rincón perfecto para los amantes de la fotografía. Gracias a
            los diversos recursos que ofrecemos, así como las características de la página, hacen de Photomancy
            el lugar ideal para comenzar (o seguir avanzando) con tus habilidades fotográficas. Encontrarás a un
            montón de usuarios con ideas y dudas iguales a las tuyas y una tienda que sin duda te dará un
            empujón a la hora de mejorar tus fotografías.</p>
    </div>
    <div class="social-wrapper">
        <ul>
            <li>
                <a href="#" target="_blank">
                    <img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/twitter-128.png" alt="Twitter Logo"
                         class="twitter-icon"></a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <img src="https://www.mchenryvillage.com/images/instagram-icon.png" alt="Instagram Logo"
                         class="instagram-icon"></a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <img src="http://www.iconarchive.com/download/i54037/danleech/simple/facebook.ico"
                         alt="Facebook Logo" class="facebook-icon"></a>
            </li>
            <li>
                <a href="#" target="_blank">
                    <img src="http://icons.iconarchive.com/icons/marcus-roberto/google-play/256/Google-plus-icon.png"
                         alt="Googleplus Logo" class="googleplus-icon"></a>
            </li>
        </ul>
    </div>

    <div class="footer">
        <p>Copyright &copy; 2020 - Photmancy. All rights reserved.</p>
    </div>
</footer>
