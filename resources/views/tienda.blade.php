<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <title>Sign In</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="title" content="Formulario de ingreso en Photomancy">
    <meta name="description"
          content="Formularios de acceso y registro como usuario a Photomancy">
    <meta name="keywords" content="photomancy, formulario, registro, sesion, fotografia, imagen">
    <meta name="author" content="Adrian Moreno Rodriguez">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('css/estilos.css')}}">
    <script src="js/scripts.js"></script>
</head>
<body>
@include('includes.nav')
<div class="wrap">
    <div class="productArea">
        <div class="row">
            <div class="prod">
                <img src="https://www.fotocasion.es/static/img/articulos/50385_ciVIOPT.jpg"
                     alt="">
                <div class="infor">
                    <a href="#">
                        <h4>Cuerpo CANON EOS 90d</h4>
                    </a>
                    <p>Una cámara réflex repleta de funciones que te permite acercarte más, disparar más rápido y captar
                        increíbles imágenes de 32,5 megapíxeles.</p>
                    <h3 class="price">1.250€</h3>
                </div>

            </div>
        </div>
    
        <div class="row">
            <div class="prod">
                <img src="https://www.fotocasion.es/static/img/articulos/40256-estuche-aerfeis-nb-0021-negro-FOTOCASION-OFERTA-DESCUENTO.jpg"
                     alt="">
                <div class="infor">
                    <a href="#">
                        <h4>Estuche AERFEIS NB-0021 Negro</h4>
                    </a>
                    <p>Estuche compacto con espacio para cámara, dos objetivos, teleobjetivo y flash dinámico. </p>
                    <h3 class="price">15€</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">

    <div class="productArea">
        <div class="row">
            <div class="prod">
                <img src="https://www.fotocasion.es/static/img/articulos/43495-estuche-gopro-cam-soportes-acc-FOTOCASION-OFERTA-DESCUENTO.jpg"
                     alt="">
                <div class="infor">
                    <a href="#">
                        <h4>Estuche GoPro (Cam + Soportes + Accesorios) ABSsc-001</h4>
                    </a>
                    <p>Casey es la solución perfecta de viaje y almacenamiento para tu GoPro. Es lo suficientemente
                        pequeña para llevarla en una mochila, pero lo suficientemente grande para almacenar tus cámaras
                        HERO, además de los soportes y accesorios básicos.</p>
                    <h3 class="price">60€</h3>
                </div>

            </div>
        
        </div>
        <div class="row">
            <div class="prod">
                <img src="https://www.fotocasion.es/static/img/articulos/48302-objetivo-nikon-afs-180-4004-e--FOTOCASION-OFERTA-DESCUENTO.jpg"
                     alt="">
                <div class="infor">
                    <a href="#">
                        <h4>Objetivo NIKON AFS180-400/4 E TC1.4 FL ED VR</h4>
                    </a>
                    <p>Definición en todo momento. Estabilidad en todo momento. Puede ampliar la distancia focal de
                        180-400 mm (f/4) hasta 560 mm (f/5.6) sin tener que cambiar la forma en la que sujeta la cámara.
                        Puede activar el teleconversor con el dedo mediante una palanca sólida y bien posicionada, para
                        no dejar de mirar por el visor.</p>
                    <h3 class="price">11.600€</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@include('includes.footer')

</body>
</html>