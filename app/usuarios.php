<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuarios extends Model
{
    use HasFactory;
    protected $table = 'usuarios';
    protected $fillable = [
        'nombre_usuario','apellidos_usuario', 'contrasena_usuario','email_usuario', 'alias_usuario','foto_usuario', 'permiso_usuario', 'id_tutorial'
    ];
}
