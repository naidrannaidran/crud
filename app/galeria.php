<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class galeria extends Model
{
    use HasFactory;
    protected $table = 'galeria';
    protected $fillable = [
        'nombre_foto','ruta_foto','id_ruta'
    ];
}
