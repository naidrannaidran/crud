<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PublicController extends Controller
{
    public function listarUsuarios()
    {        
        $usuario = DB::table('usuarios')->get();
        return view('usuarios', compact('usuarios'));
    }
    public function listarGaleria()
    {        
        $galeria = DB::table('galeria')->get();
        return view('galeria', compact('galeria'));
    }
    public function listarRutas()
    {        
        $rutas = DB::table('rutas')->get();
        return view('rutas', compact('rutas'));
    }
    public function listarTecnicas()
    {        
        $tecnica = DB::table('tecnica')->get();
        return view('tecnicas', compact('tecnica'));
    }
    public function listarTutoriales()
    {        
        $tutoriales = DB::table('tutoriales')->get();
        return view('tutoriales', compact('tutoriales'));
    }
    public function listarTienda()
    {        
        $tienda = DB::table('tienda')->get();
        return view('tienda', compact('tienda'));
    }
}
