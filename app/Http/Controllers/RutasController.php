<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class rutasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rutas = DB::table('rutas')->get();
        return view('modelo.Rutas.rutas-index', compact('rutas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.Rutas.rutas-create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('rutas')->insert(
            array(
                "nombre_ruta" => $request->input('nombre_ruta'),
                "descripcion_ruta" => $request->input('descripcion_ruta'),
                "mapa_ruta" => $request->file('mapa_ruta')->store('imgs','public'),
            )
        );
        return redirect()->action('RutasController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rutas = DB::table('rutas')->where('id' , '=', $id)->first();
        return view('modelo.Rutas.rutas-show', compact('rutas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rutas = DB::table('rutas')->where('id' , '=', $id)->first();
        return view('modelo.Rutas.rutas-edit', compact('rutas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('mapa_ruta');
        if($imagen){
            DB::table('rutas')->where('id', '=', $id)->update(
                array(
                    "nombre_ruta" => $request->input('nombre_ruta'),
                    "descripcion_ruta" => $request->input('descripcion_ruta'),
                    "mapa_ruta" => $request->file('mapa_ruta')->store('imgs','public'),
                )
            );
        }else{
            DB::table('rutas')->where('id', '=', $id)->update(
                array(
                    "nombre_ruta" => $request->input('nombre_ruta'),
                    "descripcion_ruta" => $request->input('descripcion_ruta'),
                )
            );
        }
        return redirect()->action('RutasController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('rutas')->where('id' , '=', $id)->delete();
        return redirect()->back();
    }
}
