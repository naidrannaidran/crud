<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class tiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tienda = DB::table('tienda')->get();
        return view('modelo.Tienda.tienda-index', compact('tienda'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.Tienda.tienda-view');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('tienda')->insert(
            array(
                "nombre_articulo" => $request->input('nombre_articulo'),
                "descripcion_articulo" => $request->input('descripcion_ruta'),
                "foto_articulo" => $request->file('foto_articulo')->store('imgs','public'),
                "precio_articulo" => $request->input('precio_articulo'),
            )
        );
        return redirect()->action('TiendaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tienda = DB::table('tienda')->where('id' , '=', $id)->first();
        return view('modelo.Tienda.tienda-show', compact('tienda'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tienda = DB::table('tienda')->where('id' , '=', $id)->first();
        return view('modelo.Tienda.tienda-edit', compact('tienda'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('foto_articulo');
        if($imagen){
            DB::table('tienda')->where('id', '=', $id)->update(
                array(
                    "nombre_articulo" => $request->input('nombre_articulo'),
                    "descripcion_articulo" => $request->input('descripcion_ruta'),
                    "foto_articulo" => $request->file('foto_articulo')->store('imgs','public'),
                    "precio_articulo" => $request->input('precio_articulo'),
                )
            );
        }else{
            DB::table('tienda')->where('id', '=', $id)->update(
                array(
                    "nombre_articulo" => $request->input('nombre_articulo'),
                    "descripcion_articulo" => $request->input('descripcion_ruta'),
                    "precio_articulo" => $request->input('precio_articulo'),
                )
            );
        }
        return redirect()->action('TiendaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tienda')->where('id' , '=', $id)->delete();
        return redirect()->back();
    }
}
