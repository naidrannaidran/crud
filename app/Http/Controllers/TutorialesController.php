<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class tutorialesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tutoriales = DB::table('tutoriales')->get();
        return view('modelo.Tutoriales.tutoriales-index', compact('tutoriales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.Tutoriales.tutoriales-view');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('tienda')->insert(
            array(
                "nombre_tutorial" => $request->input('nombre_tutorial'),
                "descripcion_tutorial" => $request->input('descripcion_tutorial'),
                "video_tutorial" => $request->file('video_tutorial')->store('imgs','public'),

            )
        );
        return redirect()->action('TutorialesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tutoriales = DB::table('tutoriales')->where('id' , '=', $id)->first();
        return view('modelo.Tutoriales.tutoriales-show', compact('tutoriales'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tutoriales = DB::table('tutoriales')->where('id' , '=', $id)->first();
        return view('modelo.Tutoriales.tutoriales-edit', compact('tutoriales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('tienda')->where('id', '=', $id)->update(
            array(
                "nombre_tutorial" => $request->input('nombre_tutorial'),
                "descripcion_tutorial" => $request->input('descripcion_tutorial'),
                "video_tutorial" => $request->file('video_tutorial')->store('imgs','public'),
            )
        );
        return redirect()->action('TutorialesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tutoriales')->where('id' , '=', $id)->delete();
        return redirect()->back();
    }
}
