<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::where('user_id', Auth::user()->id)->get();
        return view('files.files-index', compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        $imagen_subida = $request->file('file');
        $nombre = $imagen_subida->getClientOriginalName();
        $type = $imagen_subida->getClientOriginalExtension();
        $folder = Auth::user()->id . "-" . Auth::user()->name;

        if(!Storage::disk('public')->exists($folder . '/' . Auth::user()->id . "-" . $nombre)){
            Storage::disk('public')->putFileAs($folder, $imagen_subida, Auth::user()->id . "-" . $nombre);
        
            $file = new File();
            $file::create([
                'name' => Auth::user()->id . "-" . $nombre,
                'type' => $type,
                'folder' => $folder,
                'user_id' => Auth::user()->id
            ]);

            $notification = array(
                'message' => 'Archivo subido correctamente',
                'alert-type' => 'success'
            );
        
        }else{
            $notification = array(
				'message' => 'Ya existe este archivo',
				'alert-type' => 'error'
			);
        }
        
        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::find($id);
        $folder = Auth::user()->id . "-" . Auth::user()->name;
        if(Storage::disk('public')->exists($folder . '/' . Auth::user()->id . "-" . $file->name)){
            Storage::disk('public')->delete($folder . '/' . $file->name);
            $file->delete();
            $notification = array(
                'message' => 'Archivo eliminado correctamente',
                'alert-type' => 'success'
            );
        }else{
            $notification = array(
				'message' => 'No existe archivo',
				'alert-type' => 'error'
			);
        }
        return redirect()->back()->with($notification);
    }
}
