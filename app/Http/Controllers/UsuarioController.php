<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = DB::table('usuarios')->get();
        return view('modelo.Usuario.usuario-index', compact('usuario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.Usuario.usuario-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('usuarios')->insert(
            array(
                "nombre_usuario" => $request->input('nombre_usuario'),
                "apellidos_usuario" => $request->input('apellidos_usuario'),
                "contrasena_usuario" => $request->input('contrasena_usuario'),
                "alias_usuario" => $request->input('alias_usuario'),
                "foto_usuario" => $request->input('foto_usuario'),
                "permiso_usuario" => $request->input('permiso_usuario'),
                "id_tutorial" => $request->input('id_tutorial'),
            )
        );
        return redirect()->action('UsuarioController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = DB::table('usuarios')->where('id' , '=', $id)->first();
        return view('modelo.Usuario.usuario-show', compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = DB::table('usuarios')->where('id' , '=', $id)->first();
        return view('modelo.Usuario.usuario-edit', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('usuarios')->where('id', '=', $id)->update(
            array(
                "nombre_usuario" => $request->input('nombre_usuario'),
                "apellidos_usuario" => $request->input('apellidos_usuario'),
                "contrasena_usuario" => $request->input('contrasena_usuario'),
                "alias_usuario" => $request->input('alias_usuario'),
                "foto_usuario" => $request->input('foto_usuario'),
                "permiso_usuario" => $request->input('permiso_usuario'),
                "id_tutorial" => $request->input('id_tutorial'),
            )
        );
        return redirect()->action('UsuarioController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('usuarios')->where('id' , '=', $id)->delete();
        return redirect()->back();
    }
}
