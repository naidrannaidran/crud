<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class galeriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galeria = DB::table('galeria')->get();
        return view('modelo.Galeria.galeria-index', compact('galeria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.Galeria.galeria-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('galeria')->insert(
            array(
                "nombre_foto" => $request->input('nombre_foto'),
                "ruta_foto" => $request->file('ruta_foto')->store('imgs','public'),
                "id_ruta" => $request->input('id_ruta'),
                )
        );
        return redirect()->action('GaleriaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $galeria = DB::table('galeria')->where('id' , '=', $id)->first();
        return view('modelo.Galeria.galeria-show', compact('galeria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeria = DB::table('galeria')->where('id' , '=', $id)->first();
        return view('modelo.Galeria.galeria-edit', compact('galeria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imagen=$request->file('ruta_foto');
        if($imagen){
            DB::table('galeria')->where('id', '=', $id)->update(
                array(
                    "nombre_foto" => $request->input('nombre_foto'),
                    "ruta_foto" => $request->file('ruta_foto')->store('imgs','public'),
                    "id_ruta" => $request->input('id_ruta'),
                )
            );
        }else{
            DB::table('galeria')->where('id', '=', $id)->update(
                array(
                    "nombre_foto" => $request->input('nombre_foto'),
                    "id_ruta" => $request->input('id_ruta'),
                )
            );
        }
 
        return redirect()->action('GaleriaController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('galeria')->where('id' , '=', $id)->delete();
        return redirect()->back();
    }
}
