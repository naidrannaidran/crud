<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;
class tecnicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tecnica = DB::table('tecnica')->get();
        return view('modelo.Tecnica.tecnica-index', compact('tecnica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelo.Tecnica.tecnica-view');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('tecnica')->insert(
            array(
                "nombre_tecnica" => $request->input('nombre_tecnica'),
                "descripcion_tecnica" => $request->input('descripcion_tecnica'),
                "foto_tecnica" => $request->file('foto_tecnica')->store('imgs','public'),
            )
        );
        return redirect()->action('TecnicaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tecnica = DB::table('tecnica')->where('id' , '=', $id)->first();
        return view('modelo.Tecnica.tecnica-show', compact('tecnica'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tecnica = DB::table('tecnica')->where('id' , '=', $id)->first();
        return view('modelo.Tecnica.tecnica-edit', compact('tecnica'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('tecnica')->where('id', '=', $id)->update(
            array(
                "nombre_tecnica" => $request->input('nombre_tecnica'),
                "descripcion_tecnica" => $request->input('descripcion_tecnica'),
                "foto_tecnica" => $request->file('foto_tecnica')->store('imgs','public'),
            )
        );
        return redirect()->action('TecnicaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('tecnica')->where('id' , '=', $id)->delete();
        return redirect()->back();
    }
}
