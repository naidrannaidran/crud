<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rutas extends Model
{
    use HasFactory;
    protected $table = 'rutas';
    protected $fillable = [
        'nombre_ruta','descripcion_ruta','mapa_ruta'
    ];
}
