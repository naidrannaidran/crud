<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tecnica extends Model
{
    use HasFactory;
    protected $table = 'tecnica';
    protected $fillable = [
        'nombre_tecnica','descripcion_tecnica','foto_tecnica'
    ];
}
