<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tutoriales extends Model
{
    use HasFactory;
    protected $table = 'tutoriales';
    protected $fillable = [
        'nombre_tutorial','descripcion_tutorial','video_tutorial'
    ];
}
