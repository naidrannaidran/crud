<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tienda extends Model
{
    use HasFactory;
    protected $table = 'tienda';
    protected $fillable = [
        'nombre_articulo','descripcion_articulo','foto_articulo', 'precio_articulo'
    ];
}
